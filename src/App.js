import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import User from './User';
import UserAddForm from './UserAddForm';

class App extends Component {
  constructor(props) {
    super(props);
    this.removeUser = this.removeUser.bind(this);
    this.state = {users: [{id: 1, name: 'Denis'}]};
  }

  removeUser(userId) {
    let userIndex = this.state.users.map((user) => {
      return user.id;
    }).indexOf(userId);

    this.setState({
      users: this.state.users.filter((_, i) => i !== userIndex)
    });
  }

  addUser(user) {
    let users = this.state.users.slice();
    users.push(user);
    this.setState({users: users});
  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h2>Users</h2>
        </div>
        <div className="App-intro">
          <div className="container">
            {this.state.users.map(user => (
                <User user={user} onUserRemove={this.removeUser.bind(this)} key={user.id} />
            ))}

            <UserAddForm onUserAdd={this.addUser.bind(this)} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
