import React, { Component } from 'react';

class UserAddForm extends Component {
    constructor(props) {
        super(props);
        this.addUser = this.addUser.bind(this);
        this.state = {newUserName: ''};
    }

    addUser(event) {
        event.preventDefault();

        if (!this.state.newUserName) {
            return;
        }

        let newUser = {
            name: this.state.newUserName,
            id: this.makeUid()
        };

        this.props.onUserAdd(newUser);
        this.setState({newUserName: ''});
    }

    makeUid() {
        let text = "";
        let possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

        for (var i = 0; i < 5; i++)
            text += possible.charAt(Math.floor(Math.random() * possible.length));

        return text;
    }

    handleNewUserNameChange(event) {
        this.setState({newUserName: event.target.value});
    }

    render() {
        return (
            <form className="form-inline">
                <div className="form-group">
                    <input type="text" value={this.state.newUserName} onChange={this.handleNewUserNameChange.bind(this)} className="form-control" placeholder="Name" />
                </div>
                <button className="btn btn-default" onClick={this.addUser}>Add User</button>
            </form>
        );
    }
}

export default UserAddForm;
