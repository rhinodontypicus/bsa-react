import React, { Component } from 'react';

class User extends Component {
    removeUser() {
        this.props.onUserRemove(this.props.user.id);
    }

    render() {
        return (
            <li className="list-group-item">
                {this.props.user.name} [id: {this.props.user.id}]
                <a className="btn btn-danger btn-xs" onClick={this.removeUser.bind(this)} role="button">Remove</a>
            </li>
        );
    }
}

export default User;
